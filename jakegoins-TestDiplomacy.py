#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):     # army holding
        in_file = StringIO("A Madrid Hold\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A Madrid\n")
        
    def test_solve_2(self):     # 2 armies fighting and dying
        in_file = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A [dead]\nB [dead]\n")
        
    def test_solve_3(self):     # 2 armies fighting, 1 gets support and lives
        in_file = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A [dead]\nB Madrid\nC London\n")
    
    def test_solve_4(self):     # army moving
        in_file = StringIO("A Madrid Move London\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A London\n")
    
    def test_solve_5(self):     # 2 armies swapping places
        in_file = StringIO("A Madrid Move London\nB London Move Madrid\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A London\nB Madrid\n")

    def test_solve_6(self):     # army holding and getting supported
        in_file = StringIO("A Madrid Hold\nB London Support A\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A Madrid\nB London\n")
    
    def test_solve_7(self):     # 2 armies fighting, 1 gets supported but the supporter get attacked
        in_file = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_8(self):     # 3 armies converge and fight at 1 city 
        in_file = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_9(self):     # 3 armies fight, 2 get supported, all still die
        in_file = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        out_file = StringIO()
        diplomacy_solve(in_file,out_file)
        self.assertEqual(out_file.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

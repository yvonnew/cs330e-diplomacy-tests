#!/usr/bin/env python3

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------

class TestDiplomacy(TestCase):
    #----
    #read
    #----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i,  ['A', 'Madrid', 'Hold', None])
    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i, ['B', 'Barcelona', 'Move', 'Madrid'])

    #----
    #eval
    #----

    def test_eval_1(self):
        game = [['A', 'Madrid', 'Hold', None],
                ['B', 'Barcelona', 'Move', 'Madrid'],
                ['C', 'London', 'Support', 'B']
            ]
        game_out = diplomacy_eval(game)
        self.assertEqual(game_out, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])

    #-----
    #print
    #-----

    def test_print_1(self):
        w = StringIO()
        game_out = [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']]
        diplomacy_print(w, game_out)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")


    #-----
    #solve
    #-----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()